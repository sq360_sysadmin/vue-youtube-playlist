# component-youtube-playlist

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```

### Run your tests
```
yarn run test
```

### Lints and fixes files
```
yarn run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

## Sample use for the Library compile
see the dist/demo.thml file for eample of using the custom tag 

The css provided there is for dev/testing purposes only. DO NOT USE in production